from django.urls import path
from core import views

app_name = "core"

urlpatterns = [
    ## Estadio:
    path('list/estadio/', views.ListEstadio.as_view(), name="EstadioList"),
    path('create/estadio/', views.CreateEstadio.as_view(), name="EstadioCreate"),
    path('detail/estadio/<int:pk>/', views.DetailEstadio.as_view(), name="EstadioDetail"),
    path('update/estadio/<int:pk>/', views.UpdateEstadio.as_view(), name="EstadioUpdate"),
    path('delete/estadio/<int:pk>/', views.DeleteEstadio.as_view(), name="EstadioDelete"),

    ## Ciudad:
    path('list/ciudad/', views.ListCiudad.as_view(), name="CiudadList"),
    path('create/ciudad/', views.CreateCiudad.as_view(), name="CiudadCreate"),
    path('detail/ciudad/<int:pk>/', views.DetailCiudad.as_view(), name="CiudadDetail"),
    path('update/ciudad/<int:pk>/', views.UpdateCiudad.as_view(), name="CiudadUpdate"),
    path('delete/ciudad/<int:pk>/', views.DeleteCiudad.as_view(), name="CiudadDelete"),

    ## Equipo:
    path('list/equipo/', views.ListEquipo.as_view(), name="EquipoList"),
    path('create/equipo/', views.CreateEquipo.as_view(), name="EquipoCreate"),
    path('detail/equipo/<int:pk>/', views.DetailEquipo.as_view(), name="EquipoDetail"),
    path('update/equipo/<int:pk>/', views.UpdateEquipo.as_view(), name="EquipoUpdate"),
    path('delete/equipo/<int:pk>/', views.DeleteEquipo.as_view(), name="EquipoDelete"),
]
