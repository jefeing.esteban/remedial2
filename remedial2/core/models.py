from django.db import models

# Create your models here.

from django.db.models.signals import pre_save
from django.dispatch import receiver

## -------------------------------- TEAM-NFL -------------------------------- ##

## CIUDADES:
class Ciudad(models.Model):
    NombreCiudad = models.CharField(max_length=80, blank=False, null=False)
    DescripcionCiudad = models.CharField(max_length=120, blank=False, null=False)

    def __str__(self):
        return self.NombreCiudad

## ESTADIOS:
class Estadio(models.Model):
    NombreEstadio = models.CharField(max_length=80, blank=False, null=False)
    DescripcionEstadio = models.CharField(max_length=120, blank=False, null=False)
    Ciudad = models.ForeignKey(Ciudad, on_delete=models.CASCADE)

    def __str__(self):
        return self.NombreEstadio

## EQUIPOS:
class Equipo(models.Model):
    NombreEquipo = models.CharField(max_length=80, blank=False, null=False)
    DescripcionEquipo = models.CharField(max_length=120, blank=False, null=False)
    Estadio = models.ForeignKey(Estadio, on_delete=models.CASCADE, blank=False, null=False)
    Ciudad = models.ForeignKey(Ciudad, on_delete=models.CASCADE, default=None)

    def __str__(self):
        return self.NombreEquipo

## Asignación automática de una ciudad.
@receiver(pre_save, sender=Equipo)
def AsignacionCiudad(sender, instance, **kwargs):
    if not getattr(instance, '_updating', False):
        instance._updating = True

        if instance.Estadio and instance.Estadio.Ciudad:
            instance.Ciudad = instance.Estadio.Ciudad
            instance.save()

        instance._updating = False
