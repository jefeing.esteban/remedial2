from django.views import generic
from django.shortcuts import render
from django.urls import reverse_lazy

from .models import Ciudad, Estadio, Equipo
from .forms import CiudadForm, EstadioForm, EquipoForm, UpdateCiudadForm, UpdateEstadioForm, UpdateEquipoForm

# Create your views here.

## CREATE:
class CreateEstadio(generic.CreateView):
    model = Estadio
    form_class = EstadioForm
    template_name = "core/EstadioCreate.html"
    success_url = reverse_lazy('core:EstadioList')

## RETRIEVE (LIST - ESTADIO):
class ListEstadio(generic.View):
    template_name = "core/EstadioList.html"

    def get(self, request, *args, **kwargs):
        queryset = Estadio.objects.all()

        self.context = {
            "EstadioQ": queryset
        }
        return render(request, self.template_name, self.context)

## UPDATE:
class UpdateEstadio(generic.UpdateView):
    model = Estadio
    form_class = UpdateEstadioForm
    template_name = "core/EstadioUpdate.html"
    success_url = reverse_lazy("core:EstadioList")

## DELETE:
class DeleteEstadio(generic.DeleteView):
    model = Estadio
    template_name = "core/EstadioDelete.html"
    success_url = reverse_lazy("core:EstadioList")

## DETAIL:
class DetailEstadio(generic.DetailView):
    template_name = "core/EstadioDetail.html"
    model = Estadio

## CIUDAD: ------------------------------------------------------------------ ##

## CREATE:
class CreateCiudad(generic.CreateView):
    model = Ciudad
    form_class = CiudadForm
    template_name = "core/CiudadCreate.html"
    success_url = reverse_lazy('core:CiudadList')

## RETRIEVE (LIST - ESTADIO):
class ListCiudad(generic.View):
    template_name = "core/CiudadList.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Ciudad.objects.all()

        self.context = {
            "CiudadQ" : queryset
        }
        return render(request, self.template_name, self.context)

## UPDATE:
class UpdateCiudad(generic.UpdateView):
    model = Ciudad
    form_class = UpdateCiudadForm
    template_name = "core/CiudadUpdate.html"
    success_url = reverse_lazy("core:CiudadList")

## DELETE:
class DeleteCiudad(generic.DeleteView):
    model = Ciudad
    template_name = "core/CiudadDelete.html"
    success_url = reverse_lazy("core:CiudadList")

## DETAIL:
class DetailCiudad(generic.DetailView):
    template_name = "core/CiudadDetail.html"
    model = Ciudad

## EQUIPO: ------------------------------------------------------------------ ##

class CreateEquipo(generic.CreateView):
    model = Equipo
    form_class = EquipoForm
    template_name = "core/EquipoCreate.html"
    success_url = reverse_lazy('core:EquipoList')

class ListEquipo(generic.View):
    template_name = "core/EquipoList.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Equipo.objects.all()

        self.context = {
            "EquipoQ" : queryset
        }
        return render(request, self.template_name, self.context)

class UpdateEquipo(generic.UpdateView):
    model = Equipo
    form_class = UpdateEquipoForm
    template_name = "core/EquipoUpdate.html"
    success_url = reverse_lazy('core:EquipoList')

class DeleteEquipo(generic.DeleteView):
    model = Equipo
    template_name = "core/EquipoDelete.html"
    success_url = reverse_lazy('core:EquipoList')

## DETAIL:
class DetailEquipo(generic.DetailView):
    template_name = "core/EquipoDetail.html"
    model = Equipo
